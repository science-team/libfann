#!/bin/sh
# autopkgtest check: Build and run a program against libfann, to verify that the
# headers are installed correctly
# Source taken from http://leenissen.dk/fann/wp/help/getting-started/
# (C) 2013 Vibhav Pant
# Author: Vibhav Pant <vibhavp@ubuntu.com>
#
# Updated by Christian Kastner <ckk@debian.org> to use $AUTOPKGTEST_TMP.

set -e

# Presence of $AUTOPKGTEST_TMP implies that someone will handle cleanup for us,
# so we can avoid duplicating the effort (signal handling, etc.) here.
if [ -z "$AUTOPKGTEST_TMP" ]
then
	echo "Required envvar \"$AUTOPKGTEST_TMP\"is not set" >&2
	exit 1
fi

cd "$AUTOPKGTEST_TMP"
cat <<EOF > libfann_test.c
#include <fann.h>

int main(void)
{
    const unsigned int num_input = 2;
    const unsigned int num_output = 1;
    const unsigned int num_layers = 3;
    const unsigned int num_neurons_hidden = 3;

    struct fann *ann = fann_create_standard(num_layers, num_input,
        num_neurons_hidden, num_output);
    fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
    fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);
    fann_destroy(ann);
    return 0;
}
EOF

gcc -o libfann_test libfann_test.c -lfann -Wall -Werror
echo "build: OK"
[ -x libfann_test ]
./libfann_test
echo "run: OK"
